
package tarea3_agustinvargasquesada;

import java.util.Scanner;


public class ejercicio3 {
    
        public void calculo ()
        {
            double salario, hora, sal_bruto, sal_neto, deducciones;
        
            Scanner empleado = new Scanner(System.in);
            System.out.print("Digite las horas laboradas: " );
            salario = empleado.nextInt();
            System.out.print("Digite el valor de la hora laborada: " );
            hora = empleado.nextInt();
            sal_bruto = salario * hora;
            sal_neto = sal_bruto - (sal_bruto * (9.17/100));
            deducciones = sal_bruto - sal_neto;
            System.out.println("El salario bruto es de " + sal_bruto);
            System.out.println("El salario neto es de " + sal_neto);
            System.out.println("Las deducciones son de " + deducciones);
        }
}
