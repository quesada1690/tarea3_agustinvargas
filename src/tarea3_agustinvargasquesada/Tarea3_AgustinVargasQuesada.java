package tarea3_agustinvargasquesada;

import java.util.Scanner;

/**
 *
 * @author varga
 */
public class Tarea3_AgustinVargasQuesada {
    
    
    public static void main(String[] args) //clase principla
    {   
        int opcion;
        Scanner eleccion = new Scanner(System.in);
        do  //menu con do/while para poder enciclar y que se salga hasta que el usuario quiera
        {
        System.out.println("Menú principal");
        System.out.println("Digite la opcion que desee utilizar");
        System.out.println("1-Promedio");
        System.out.println("2-Numero 20");
        System.out.println("3-Salario");
        System.out.println("4-Edad actual");
        System.out.println("5-Salir");
        opcion = eleccion.nextInt();
            switch (opcion)
                {
                    case 1:
                        ejercicio1 promedio = new ejercicio1();
                        promedio.promedio();
                        break;
                    
                    case 2:
                        ejercicio2 suma = new ejercicio2();
                        suma.sumatoria();
                        break;
                    
                    case 3:
                        ejercicio3 salario = new ejercicio3();
                        salario.calculo();
                        break;
                    
                    case 4:
                        ejercicio4 edad_real = new ejercicio4();
                        edad_real.años_cumplidos();
                        break;
                    
                    default:
                        System.out.println("Digite una de las opciones dadas");
                        break;
                }
        }while(opcion != 5);
        
        System.out.println("Que tenga un buen dia");
    }
    
}
